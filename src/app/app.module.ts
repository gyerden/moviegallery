import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SimpleFormComponent } from './simple-form/simple-form.component';
import { MovielistComponent } from './movielist/movielist.component';
import { MovieeditComponent } from './movieedit/movieedit.component';
import { MovieviewComponent } from './movieview/movieview.component';

@NgModule({
  declarations: [
    AppComponent,
    SimpleFormComponent,
    MovielistComponent,
    MovieeditComponent,
    MovieviewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
