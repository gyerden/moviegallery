import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movieview',
  template: `
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Your List</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Posters</a>
      </li>
    </ul>
  </div>
</nav>
   <div class="container">
   <div class="jumbotron">
   <table class="table">
   <thead>
     <tr>
       <th>Title</th>
       <th>Plot</th>
       <th>Rating</th>
       <th>Actors List</th>
     </tr>
   </thead>
   </table>
   
 </div>
  `,
  styles: []
})
export class MovieviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
