import { Component,Inject } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <div>
   <app-movielist></app-movielist>
  </div>
`,
})
export class AppComponent {
  title = 'Lets Get Started!';

  constructor()
  {

  }
}
